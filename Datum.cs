﻿using Newtonsoft.Json;
using System;

namespace Practice_07_06
{
    public class Datum
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("home_team")]
        public HomeTeam HomeTeam { get; set; }

        [JsonProperty("home_team_score")]
        public int HomeTeamScore { get; set; }

        [JsonProperty("period")]
        public int Period { get; set; }

        [JsonProperty("postseason")]
        public bool Postseason { get; set; }

        [JsonProperty("season")]
        public int Season { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }

        [JsonProperty("visitor_team")]
        public VisitorTeam VisitorTeam { get; set; }

        [JsonProperty("visitor_team_score")]
        public int VisitorTeamScore { get; set; }

        public override string ToString()
        {
            string result = $"Дата игры : {Date.ToShortDateString()}\n\nКоманда хозяев : \n{HomeTeam}\n\nКоманда гостей : \n{VisitorTeam}\n\nСчет : {HomeTeamScore}:{VisitorTeamScore}";
            return result;
        }
    }
}
