﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RapidAPISDK;
using unirest_net.http;

namespace Practice_07_06
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Task.Run<HttpResponse<string>>(() =>
            {
                var res = Unirest.get("https://free-nba.p.rapidapi.com/games?page=0&per_page=25")
                .header("X-RapidAPI-Host", "free-nba.p.rapidapi.com")
                .header("X-RapidAPI-Key", "ace0a9637cmsh29b6972819e3e17p1cd240jsn8ba409d36fb3")
                .asJson<string>();
                return res;
            });

            #region design
            List<char> loadingChars = new List<char>();
            loadingChars.Add('-');
            loadingChars.Add('\\');
            loadingChars.Add('|');
            loadingChars.Add('/');

            int i = 0;
            while(!result.IsCompleted)
            {
                Console.Clear();
                Console.WriteLine($"Загрузка {loadingChars[i]}");
                Thread.Sleep(100);
                i++;
                if (i == loadingChars.Count) i = 0;
            }
            Console.Clear();
            #endregion

            if(result.Result.Code == 200)
            {
                Games games = JsonConvert.DeserializeObject<Games>(result.Result.Body);
                foreach (var game in games.Data)
                {
                    Console.WriteLine(game);
                    Console.WriteLine("-------------------------");
                }
            }
            else
            {
                Console.WriteLine("Произошла непредвиденная ошибка!");
            }
            Console.ReadLine();
        }
    }
}
