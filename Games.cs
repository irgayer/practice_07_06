﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_07_06
{
    public class Games
    {
        [JsonProperty("data")]
        public IList<Datum> Data { get; set; }
    }
}
